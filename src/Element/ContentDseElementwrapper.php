<?php

/**
 * 361GRAD Element Productoverview
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementElementwrapper\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\File;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\StringUtil;
use Patchwork\Utf8;

/**
 * Class ContentDseElementwrapper
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseElementwrapper extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_productoverview';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
        // Build subheadline like Contao headline
        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';

        $filterClass1 = array();
        if ($this->dse_firstTileWidth1) array_push($filterClass1, ' 100s');
        if ($this->dse_firstTileWidth2) array_push($filterClass1, ' 200s');
        if ($this->dse_firstTileWidth3) array_push($filterClass1, ' 300s');
        if ($this->dse_firstTileWidth4) array_push($filterClass1, ' 400s');
        if ($this->dse_firstTileWidth5) array_push($filterClass1, ' 500s');
        if ($this->dse_firstTileWidth6) array_push($filterClass1, ' mixed');
        if ($this->dse_firstTileWoodType1) array_push($filterClass1, ' douglasie');
        if ($this->dse_firstTileWoodType2) array_push($filterClass1, ' eiche');
        if ($this->dse_firstTileLong1) array_push($filterClass1, ' fallend');
        if ($this->dse_firstTileLong2) array_push($filterClass1, ' raumlang');
        if ($this->dse_firstTileSurface1) array_push($filterClass1, ' geseift');
        if ($this->dse_firstTileSurface2) array_push($filterClass1, ' geolt');
        if ($this->dse_firstTileSurface3) array_push($filterClass1, ' lackiert');
        if ($this->dse_firstTileSorting1) array_push($filterClass1, ' select');
        if ($this->dse_firstTileSorting2) array_push($filterClass1, ' natur');
        if ($this->dse_firstTileType1) array_push($filterClass1, ' residential');
        if ($this->dse_firstTileType2) array_push($filterClass1, ' gallery');
        if ($this->dse_firstTileType3) array_push($filterClass1, ' office');
        if ($this->dse_firstTileType4) array_push($filterClass1, ' medical');
        if ($this->dse_firstTileType5) array_push($filterClass1, ' hotel-gastro');
        if ($this->dse_firstTileType6) array_push($filterClass1, ' showroom');
        $this->Template->fc1         = $filterClass1;
        $filterClass2 = array();
        if ($this->dse_secondTileWidth1) array_push($filterClass2, ' 100s');
        if ($this->dse_secondTileWidth2) array_push($filterClass2, ' 200s');
        if ($this->dse_secondTileWidth3) array_push($filterClass2, ' 300s');
        if ($this->dse_secondTileWidth4) array_push($filterClass2, ' 400s');
        if ($this->dse_secondTileWidth5) array_push($filterClass2, ' 500s');
        if ($this->dse_secondTileWidth6) array_push($filterClass2, ' mixed');
        if ($this->dse_secondTileWoodType1) array_push($filterClass2, ' douglasie');
        if ($this->dse_secondTileWoodType2) array_push($filterClass2, ' eiche');
        if ($this->dse_secondTileLong1) array_push($filterClass2, ' fallend');
        if ($this->dse_secondTileLong2) array_push($filterClass2, ' raumlang');
        if ($this->dse_secondTileSurface1) array_push($filterClass2, ' geseift');
        if ($this->dse_secondTileSurface2) array_push($filterClass2, ' geolt');
        if ($this->dse_secondTileSurface3) array_push($filterClass2, ' lackiert');
        if ($this->dse_secondTileSorting1) array_push($filterClass2, ' select');
        if ($this->dse_secondTileSorting2) array_push($filterClass2, ' natur');
        if ($this->dse_secondTileType1) array_push($filterClass2, ' residential');
        if ($this->dse_secondTileType2) array_push($filterClass2, ' gallery');
        if ($this->dse_secondTileType3) array_push($filterClass2, ' office');
        if ($this->dse_secondTileType4) array_push($filterClass2, ' medical');
        if ($this->dse_secondTileType5) array_push($filterClass2, ' hotel-gastro');
        if ($this->dse_secondTileType6) array_push($filterClass2, ' showroom');
        $this->Template->fc2         = $filterClass2;
        $filterClass3 = array();
        if ($this->dse_thirdTileWidth1) array_push($filterClass3, ' 100s');
        if ($this->dse_thirdTileWidth2) array_push($filterClass3, ' 200s');
        if ($this->dse_thirdTileWidth3) array_push($filterClass3, ' 300s');
        if ($this->dse_thirdTileWidth4) array_push($filterClass3, ' 400s');
        if ($this->dse_thirdTileWidth5) array_push($filterClass3, ' 500s');
        if ($this->dse_thirdTileWidth6) array_push($filterClass3, ' mixed');
        if ($this->dse_thirdTileWoodType1) array_push($filterClass3, ' douglasie');
        if ($this->dse_thirdTileWoodType2) array_push($filterClass3, ' eiche');
        if ($this->dse_thirdTileLong1) array_push($filterClass3, ' fallend');
        if ($this->dse_thirdTileLong2) array_push($filterClass3, ' raumlang');
        if ($this->dse_thirdTileSurface1) array_push($filterClass3, ' geseift');
        if ($this->dse_thirdTileSurface2) array_push($filterClass3, ' geolt');
        if ($this->dse_thirdTileSurface3) array_push($filterClass3, ' lackiert');
        if ($this->dse_thirdTileSorting1) array_push($filterClass3, ' select');
        if ($this->dse_thirdTileSorting2) array_push($filterClass3, ' natur');
        if ($this->dse_thirdTileType1) array_push($filterClass3, ' residential');
        if ($this->dse_thirdTileType2) array_push($filterClass3, ' gallery');
        if ($this->dse_thirdTileType3) array_push($filterClass3, ' office');
        if ($this->dse_thirdTileType4) array_push($filterClass3, ' medical');
        if ($this->dse_thirdTileType5) array_push($filterClass3, ' hotel-gastro');
        if ($this->dse_thirdTileType6) array_push($filterClass3, ' showroom');
        $this->Template->fc3         = $filterClass3;
        $filterClass4 = array();
        if ($this->dse_fourthTileWidth1) array_push($filterClass4, ' 100s');
        if ($this->dse_fourthTileWidth2) array_push($filterClass4, ' 200s');
        if ($this->dse_fourthTileWidth3) array_push($filterClass4, ' 300s');
        if ($this->dse_fourthTileWidth4) array_push($filterClass4, ' 400s');
        if ($this->dse_fourthTileWidth5) array_push($filterClass4, ' 500s');
        if ($this->dse_fourthTileWidth6) array_push($filterClass4, ' mixed');
        if ($this->dse_fourthTileWoodType1) array_push($filterClass4, ' douglasie');
        if ($this->dse_fourthTileWoodType2) array_push($filterClass4, ' eiche');
        if ($this->dse_fourthTileLong1) array_push($filterClass4, ' fallend');
        if ($this->dse_fourthTileLong2) array_push($filterClass4, ' raumlang');
        if ($this->dse_fourthTileSurface1) array_push($filterClass4, ' geseift');
        if ($this->dse_fourthTileSurface2) array_push($filterClass4, ' geolt');
        if ($this->dse_fourthTileSurface3) array_push($filterClass4, ' lackiert');
        if ($this->dse_fourthTileSorting1) array_push($filterClass4, ' select');
        if ($this->dse_fourthTileSorting2) array_push($filterClass4, ' natur');
        if ($this->dse_fourthTileType1) array_push($filterClass4, ' residential');
        if ($this->dse_fourthTileType2) array_push($filterClass4, ' gallery');
        if ($this->dse_fourthTileType3) array_push($filterClass4, ' office');
        if ($this->dse_fourthTileType4) array_push($filterClass4, ' medical');
        if ($this->dse_fourthTileType5) array_push($filterClass4, ' hotel-gastro');
        if ($this->dse_fourthTileType6) array_push($filterClass4, ' showroom');
        $this->Template->fc4         = $filterClass4;
        // Set up Images in template
        $self = $this;
        $this->Template->getImageObject = function () use ($self) {
            return call_user_func_array(array($self, 'getImageObject'), func_get_args());
        };
    }
    /**
     * Get an image object from uuid
     *
     * @param       $uuid
     * @param null  $size
     * @param null  $maxSize
     * @param null  $lightboxId
     * @param array $item
     *
     * @return \FrontendTemplate|object
     */
    public function getImageObject($uuid, $size = null, $maxSize = null, $lightboxId = null, $item = array())
    {
        global $objPage;
        if (!$uuid) {
            return null;
        }
        $image = FilesModel::findByUuid($uuid);
        if (!$image) {
            return null;
        }
        try {
            $file = new File($image->path, true);
            if (!$file->exists()) {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }
        $imageMeta = $this->getMetaData($image->meta, $objPage->language);
        if (is_string($size) && trim($size)) {
            $size = deserialize($size);
        }
        if (!is_array($size)) {
            $size = array();
        }
        $size[0] = isset($size[0]) ? $size[0] : 0;
        $size[1] = isset($size[1]) ? $size[1] : 0;
        $size[2] = isset($size[2]) ? $size[2] : 'crop';
        $image = array(
            'id'        => $image->id,
            'uuid'      => isset($image->uuid) ? $image->uuid : null,
            'name'      => $file->basename,
            'singleSRC' => $image->path,
            'size'      => serialize($size),
            'alt'       => $imageMeta['title'],
            'imageUrl'  => $imageMeta['link'],
            'caption'   => $imageMeta['caption'],
        );
        $image = array_merge($image, $item);
        $imageObject = new FrontendTemplate('dse_image_object');
        $this->addImageToTemplate($imageObject, $image, $maxSize, $lightboxId);
        $imageObject = (object) $imageObject->getData();
        if (empty($imageObject->src)) {
            $imageObject->src = $imageObject->singleSRC;
        }
        return $imageObject;
    }
}
