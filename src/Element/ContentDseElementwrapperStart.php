<?php

/**
 * 361GRAD Element Elementwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementElementwrapper\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;

/**
 * Class ContentDseElementwrapperStart
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseElementwrapperStart extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_elementwrapper_start';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            $this->strTemplate = 'be_wildcard';
            $objTemplate       = new BackendTemplate($this->strTemplate);

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
    }
}
