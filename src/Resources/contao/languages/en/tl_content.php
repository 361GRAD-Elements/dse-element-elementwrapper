<?php

/**
 * 361GRAD Element Elementwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_elementwrapper_start'] = ['Elementwrapper Wrapper Start', ''];

$GLOBALS['TL_LANG']['CTE']['dse_elementwrapper_stop'] = ['Elementwrapper Wrapper Stop', ''];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];