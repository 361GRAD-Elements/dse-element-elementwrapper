<?php

/**
 * 361GRAD Element Elementwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_elementwrapper_start'] =
    'Dse\\ElementsBundle\\ElementElementwrapper\\Element\\ContentDseElementwrapperStart';

$GLOBALS['TL_CTE']['dse_elements']['dse_elementwrapper_stop']  =
    'Dse\\ElementsBundle\\ElementElementwrapper\\Element\\ContentDseElementwrapperStop';

$GLOBALS['TL_WRAPPERS']['start'][] = 'dse_elementwrapper_start';
$GLOBALS['TL_WRAPPERS']['stop'][]  = 'dse_elementwrapper_stop';